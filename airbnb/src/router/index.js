import Vue from 'vue'
import Router from 'vue-router'
import Header from '@/components/Header'
import Navigation from '@/components/HeaderContent/Navigation'
import Section from '@/components/Section'
import placeList from '@/components/mainContent/placeList'
import Luxe from '@/components/mainContent/Luxe'
import recommend from '@/components/mainContent/recommend'
import hiddenBox from '@/components/mainContent/hiddenBox'
import hiddenNav from '@/components/HeaderContent/hiddenNav'
import arrowBox from '@/store/arrowBox'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Header',
      components: {
        header: Header,
        section: Section
      },
      children: [
        {
          name: 'Navigation',
          path: '',
          components:{
            nav: Navigation,
            takeATour: placeList,
            luxe: Luxe,
            recommend: recommend,
            hiddenBox: hiddenBox,
            hiddenNav: hiddenNav,
            arrowBox: arrowBox
          }
        }
      ]
    }
  ]
})
