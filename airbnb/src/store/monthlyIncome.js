import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const monthlyIncome = new Vuex.Store({
    state: {
        income: 1118520
    },
    getters: {
        income(state){return state.income}
    },
    mutations:{
        setIncome(state,payload){
            state.income = payload.income
        }
    },
    actions:{
        doUpdate({commit},income){
            commit('setIncome',{income})
        }
    }
})

export default {
    monthlyIncome(){
        return monthlyIncome.getters.income
    }
}