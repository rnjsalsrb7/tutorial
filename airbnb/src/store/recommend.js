const recommend = [
    {id: 1, name: '파리', image: 'http://placeimg.com/300/400/arch'},
    {id: 2, name: '뉴욕', image: 'http://placeimg.com/300/400/people'},
    {id: 3, name: '도쿄', image: 'http://placeimg.com/300/400/nature'},
    {id: 4, name: '홍콩', image: 'http://placeimg.com/300/400/tech'},
    {id: 5, name: '서울', image: 'http://placeimg.com/300/400/tech'},
    {id: 6, name: '바르셀로나', image: 'http://placeimg.com/300/400/tech'},
    {id: 7, name: '런던', image: 'http://placeimg.com/300/400/tech'},
    {id: 8, name: '평양', image: 'http://placeimg.com/300/400/tech'},
    {id: 9, name: '로마', image: 'http://placeimg.com/300/400/tech'},
    {id: 10, name: 'LA', image: 'http://placeimg.com/300/400/tech'}
]

export default{
    recommend(){
        return recommend
    }
}