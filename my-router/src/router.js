/* eslint-disable */
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Product from '@/views/Product.vue'
import ProductList from '@/views/ProductList.vue'

import ProductHome from '@/views/Product/ProductHome.vue'
import ProductReview from '@/views/Product/ProductReview.vue'
import ProductReviewDetail from '@/views/Product/ProductReviewDetail.vue'

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/product',
            component: ProductList
        },
        {
            path: '/product/:id',
            component: Product,
            props: route => ({ id: Number(route.params.id) }),
            children:[
                {
                    name: 'product-home',
                    path: '',
                    component: ProductHome
                },
                {
                    name: 'product-review',
                    path: 'review',
                    component: ProductReview
                },
                {
                    name: 'review-detail',
                    path: 'review/:rid',
                    component: ProductReviewDetail
                }
            ]
        }
    ]
})

export default router