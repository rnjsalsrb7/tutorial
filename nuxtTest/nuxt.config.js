module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'nuxttest',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script:[
      {src:'https://kit.fontawesome.com/f3bc25258e.js', crossorigin: 'anonymous'},
      {src:'https://code.jquery.com/jquery-3.4.1.min.js', integrity: 'sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=', crossorigin: 'anonymous'},
      {src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAtPDQcqhJhAnJB3ZLExEbOc42Q-yajSLE&libraries=places'}
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  plugins: ['~plugins/filters.js'],
  modules:[
    '@nuxtjs/axios',
    '@nuxtjs/vuetify'
  ],
  axios:{
    //proxyHeaders: false
  },
  vuetify:{
    //Vuetify options
    // theme: {}
  },
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

