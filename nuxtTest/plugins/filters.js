import Vue from 'vue'

Vue.filter('comma', function(value){
    var num = new Number(value);
    return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g,"$1,")
})
