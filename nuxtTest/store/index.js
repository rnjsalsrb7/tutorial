import Vuex from 'vuex'

const store = () => new Vuex.Store({
    state: {
        searchText:'',
        tags: [{tag:''}],
        myBudget: 100000,
        duration: 10,
        /*mostSearched.vue Data*/
        destListPosition: 1,
        destList: [
            { id: 1, name: '대한민국 서울1', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 2, name: '대한민국 서울2', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 3, name: '대한민국 서울3', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 4, name: '대한민국 서울4', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 5, name: '대한민국 서울5', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 6, name: '대한민국 서울6', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 7, name: '대한민국 서울7', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 8, name: '대한민국 서울8', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'},
            { id: 9, name: '대한민국 서울9', eng: 'SEOUL, SOUTH KOREA', image: 'http://placeimg.com/420/650/nature'}
        ],
        /*recentlyClicked.vue Data*/
        clickedPosition: 1,
        recentlyClicked: [
            {   
                id:1,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            },
            {   
                id:2,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: false
            },
            {   
                id:3,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            },
            {   
                id:4,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: false
            },
            {   
                id:5,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            }
        ],
        /*mostCheckedWishlist.vue Data*/
        mostWishPosition: 1,
        mostWishlist: [
            {   
                id:1,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: false
            },
            {   
                id:2,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            },
            {   
                id:3,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            },
            {   
                id:4,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: false
            },
            {   
                id:5,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            }
        ],
        /*popularActivity.vue Data*/
        popularActivityPosition: 1,
        popularActivityList: [
            {   
                id:1,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: false
            },
            {   
                id:2,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: false
            },
            {   
                id:3,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            },
            {   
                id:4,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: false
            },
            {   
                id:5,
                name: '한강에서 치맥먹기',
                sub: '한강에서 치맥을 먹어보세요.',
                hash: [
                        {index: 1, name: '음식'},
                        {index: 2, name: '한강'},
                        {index: 3, name: '액티비티'}
                    ],
                image: 'http://placeimg.com/600/900/any',
                price: 1000,
                wish: true
            }
        ],
        recommendActivity:[
            {id: 1, name: '천사대교 건너가기', image: 'https://picsum.photos/5000/2000'},
            {id: 2, name: '르웨탄 유람선타기', image: 'https://picsum.photos/5000/2000'}
        ]
    },
    getters:{
        searchText(state){
            return state.searchText
        },
        tags(state){
            return state.tags
        },
        myBudget(state){
            return state.myBudget
        },
        duration(state){
            return state.duration
        },
        /*mostSearched.vue getters*/
        mostSearched(state){
            return state.destList
        },
        destPosition(state){
            return state.destListPosition
        },
        /*recentlyClicked.vue getters */
        recentlyClicked(state){
            return state.recentlyClicked
        },
        clickedPosition(state){
            return state.clickedPosition
        },
        /*mostCheckedWishlist.vue getters*/
        mostWishlist(state){
            return state.mostWishlist
        },
        mostWishPosition(state){
            return state.mostWishPosition
        },
        /*popularActivity.vue getters*/
        popularActivityList(state){
            return state.popularActivityList
        },
        popularActivityPosition(state){
            return state.popularActivityPosition
        },
        /*recommendActivity.vue*/
        recommendActivity(state){
            return state.recommendActivity
        }
    },
    mutations:{
        setSearchText(state, payload){
            //console.log(payload)
            state.searchText = payload
        },
        setTags(state,payload){
            //console.log(payload)
            state.tags = payload
        },
        setMyBudget(state,payload){
            state.myBudget = payload
        },
        setDuration(state,payload){
            state.duration = payload
        },
        /*mostSearched.vue mutations*/
        destRight(state){
            state.destListPosition++
        },
        destLeft(state){
            state.destListPosition--
        },
        setDestPosition(){
            state.destListPosition = 1
        },
        /*recentlyClicked.vue mutations*/
        clickedRight(state){
            state.clickedPosition++
        },
        clickedLeft(state){
            state.clickedPosition--
        },
        setClickedPosition(){
            state.clickedPosition = 1
        },
        clickedWishCheck(index){
            state.recentlyClicked[index].wish=!wish
        },
        /*mostCheckedWishlist.vue mutations*/
        mostWishRight(state){
            state.mostWishPosition++
        },
        mostWishLeft(state){
            state.mostWishPosition--
        },
        setmostWishPosition(){
            state.mostWishPosition = 1
        },
        mostWishWishCheck(index){
            state.mostWishlist[index].wish=!wish
        },
        /*popularActivity.vue mutations*/
        popularActivityRight(state){
            state.popularActivityPosition++
        },
        popularActivityLeft(state){
            state.popularActivityPosition--
        },
        setpopularActivityPosition(){
            state.popularActivityPosition = 1
        },
        popularActivityWishCheck(index){
            state.popularActivityList[index].wish=!wish
        }
    },
    actions:{

    }
})

export default store
