var myComponent = {
    template: '<p>{{ message }}</p>',
    data: function () {
        return {
            message: 'Hello Vue.js'
        }
    }
}
var app = new Vue({
    el: '#app',
    components: {
        'my-component': myComponent
    },
    data: {
        order: false,
        count: 2,
        ans: "",
        cur: "",
        open: 0,
        close: 0,
        list: [

        ],
        fib: [
            { id_fib: 110, fibNum: 0 }
        ],
        brackets: [
            { id: 0, left: '(', right: ')'}
        ],
        pairs: [
            { id: 1000, pair: '()'}
        ]
    },
    computed: {
        sortedList: function () {
            return _.orderBy(this.list, 'price', this.order ? 'desc' : 'asc')
        }
    },
    methods: {
        fibIn: function (count) {
            var index = count -1
            while (index < count) {
                console.log(index)
                var max = this.fib.reduce(function (a, b) {
                    return a > b.id_fib ? a : b.id_fib
                }, 0)
                var past = this.fib[max].fibNum
                var morePast = 0
                if (max == 0) {
                    this.fib.push({
                        id_fib: max + 1,
                        fibNum: 1
                    })
                } else {
                    morePast = this.fib[max - 1].fibNum
                    this.fib.push({
                        id_fib: max + 1,
                        fibNum: past + morePast
                    })
                }
                index = this.fib[max+1].fibNum + this.fib[max].fibNum
                console.log(index+'set')
            }
        },
        Reset_list: function(){
            this.fib = [
                { id_fib: 100, fibNum: 0 }
            ],
            this.brackets = [
                { id: 0, left: '(', right: ')'}
            ],
            this.pairs = [
                { id: 1000, pair: '()'}
            ],
            this.ans = ""
        },
        evenSum: function(){
            var max = this.fib.reduce(function (a, b) {
                return a > b.id_fib ? a : b.id_fib
            }, 0)
            var sum = 0
            var num=''
            for(i=0; i<=max; i++){
                if(this.fib[i].fibNum%2 == 0){
                    sum += this.fib[i].fibNum
                    num+= this.fib[i].fibNum + ' + '
                }
            }
            console.log(num.substring(0,num.length-3)+ ' = ' +sum)
        },
        make_brackets: function(count){
            var index = 0
            while (index < count) {
                var max = this.brackets.reduce(function (a, b) {
                    return a > b.id ? a : b.id
                }, 0)
                this.brackets.push({
                    id: max+1,
                    left: '(',
                    right: ')'
                })
                index++
            }
            this.brackets.splice(0,1)
        },
        recurse: function(ans, cur, open, close, length){
            if(cur.length == length*2){
                this.ans += cur+','
                console.log("final "+cur)
                return;
            }
            if(open<length){
                console.log("left do "+cur)
                this.recurse(ans, cur+"(",open+1,close,length)
            }
            if(close<open){
                console.log("right do "+cur)
                this.recurse(ans, cur+")", open, close+1, length)
            }
        },
        combination: function(){
            var length= this.brackets.length
            console.log(length)
            this.recurse(this.ans, "", 0, 0, length)
            var subs=this.ans.substring(0,this.ans.length-1)
            console.log(subs)
        }
    }
})